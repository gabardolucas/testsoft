import { 
    CARDS_FETCH_ACTION,
    CARDS_FETCH_ACTION_SUCCESS,
    CARDS_FETCH_ACTION_ERROR,
    CARDS_ADD_TAG_ACTION,
    CARDS_REMOVE_TAG_ACTION,
    CARDS_SELECT_TAG_ACTION 
} from '../actions/actionTypes';

const initialState = {
    cards: [],
    loading: false,
    error: false,
    selectedTag: null,
};

const addTagToCard = (state, tagId, cardId) => {
    const findIndex = state.cards.findIndex(card => card.id == cardId);
    const newArray = [ ...state.cards ]
    newArray[findIndex].tag.push(tagId)
    return newArray
}

const removeTagToCard = (state, tagId, cardId) => {
    const findIndex = state.cards.findIndex(card => card.id == cardId);
    const newArray = [ ...state.cards ]
    const filtered = newArray[findIndex].tag.filter(tag => tag != tagId)
    newArray[findIndex].tag = [ ...filtered ] 
    return newArray
}

export const cardsReducer = (state = initialState, action) => {
    switch (action.type) {
        case CARDS_FETCH_ACTION:
            return {
                ...state,
                loading: true,
                error: false,
            };
        case CARDS_FETCH_ACTION_SUCCESS:
            return {
                ...state,
                cards: action.cards,
                loading: false,
                error: false,
            };
        case CARDS_FETCH_ACTION_ERROR:
            return {
                ...state,
                loading: false,
                error: true,
            };
        case CARDS_ADD_TAG_ACTION:
            return {
                ...state,
                cards: addTagToCard(state, action.tagId, action.cardId),
            }
        case CARDS_REMOVE_TAG_ACTION:
            return {
                ...state,
                cards: removeTagToCard(state, action.tagId, action.cardId),
            }
        case CARDS_SELECT_TAG_ACTION:
            return {
                ...state,
                selectedTag: action.tagId,   
            }
        default:
            return state;
    }
};

export default cardsReducer;
