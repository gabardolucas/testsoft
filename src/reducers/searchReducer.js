import { SEARCH_ACTION } from '../actions/actionTypes';

const initialState = {
    searchTerm:false,
};

export const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_ACTION:
            return {
                ...state,
                searchTerm: action.searchTerm,
            };
        default:
            return state;
    }
};

export default searchReducer;
