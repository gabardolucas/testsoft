import { combineReducers } from 'redux';
import { tagsReducer } from './tagsReducer';
import { cardsReducer } from "./cardsReducer";
import { searchReducer } from "./searchReducer";

export const Reducers = combineReducers({
  	tagsReducer: tagsReducer,
  	cardsReducer: cardsReducer,
	searchReducer: searchReducer,
});