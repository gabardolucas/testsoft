import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import * as CardsEpics from '../epics/cardsEpics';
import cardsReducer from '../reducers/cardsReducer';
import * as TagsEpics from '../epics/tagsEpics';
import tagsReducer from '../reducers/tagsReducer';
import searchReducer from '../reducers/searchReducer';


const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = reduxDevTools || compose;
const rootReducer = combineReducers({ cardsReducer, tagsReducer, searchReducer });
const epics = [...Object.values(CardsEpics), ...Object.values(TagsEpics)];

export const configureStore = () => {
  const epicMiddleware = createEpicMiddleware({
    dependencies: { ajax },
  });
  const middleware = [epicMiddleware];
  const Store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(...middleware)),
  );
  const rootEpic = combineEpics(...epics);
  epicMiddleware.run(rootEpic);
  return Store;
};
