import { SEARCH_ACTION } from './actionTypes';

export const searchAction = searchTerm => ({
    type: SEARCH_ACTION,
    searchTerm,
});