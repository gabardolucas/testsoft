import { 
    CARDS_FETCH_ACTION,
    CARDS_FETCH_ACTION_SUCCESS,
    CARDS_FETCH_ACTION_ERROR,
    CARDS_ADD_TAG_ACTION,
    CARDS_REMOVE_TAG_ACTION,
    CARDS_SELECT_TAG_ACTION
} from './actionTypes';

export const fetchCardsAction = () => ({
  	type: CARDS_FETCH_ACTION
});

export const fetchCardsActionSuccess = cards => ({
	type: CARDS_FETCH_ACTION_SUCCESS,
	cards
});

export const fetchCardsActionError = () => ({
  	type: CARDS_FETCH_ACTION_ERROR
});

export const cardsAddTagAction = (tagId, cardId) => ({
  	type: CARDS_ADD_TAG_ACTION,
  	tagId,
  	cardId,
});

export const cardsRemoveTagAction = (tagId, cardId) => ({
  	type: CARDS_REMOVE_TAG_ACTION,
  	tagId,
  	cardId,
});

export const cardsSelectTagAction = tagId => ({
    type: CARDS_SELECT_TAG_ACTION,
    tagId,
});