import { of } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import * as actions from '../actions/tagsActions';

const fetchTagsAjax = () => {
  return {
    url: 'http://www.mocky.io/v2/5c46f712310000643405f615',
    body: JSON.stringify({}),
    headers: { 'Content-Type': 'application/json' },
    method: 'GET',
  };
};

export const fetchTagsEpic = (action$, state$, { ajax }) => (
  action$.pipe(
    ofType(actions.tagsFetchAction().type),
    mergeMap(action => ajax(fetchTagsAjax()).pipe(
      mergeMap(response => of(actions.tagsFetchActionSuccess(response.response.tags))),
      catchError(() => of(actions.tagsFetchActionError())),
    )),
  )
);