import { of } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import * as actions from '../actions/cardsActions';

const fetchCardsAjax = () => {
  return {
    url: 'http://www.mocky.io/v2/5c46f712310000643405f615',
    body: JSON.stringify({}),
    headers: { 'Content-Type': 'application/json' },
    method: 'GET',
  };
};

export const fetchCardsEpic = (action$, state$, { ajax }) => (
  action$.pipe(
    ofType(actions.fetchCardsAction().type),
    mergeMap(action => ajax(fetchCardsAjax()).pipe(
      mergeMap(response => of(actions.fetchCardsActionSuccess(response.response.cards))),
      catchError(() => of(actions.fetchCardsActionError())),
    )),
  )
);