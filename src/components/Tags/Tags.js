import React, { Component } from 'react';
import { connect } from 'react-redux';
import { tagsFetchAction, tagsFetchActionSuccess, tagsAddAction } from "../../actions/tagsActions";
import { cardsSelectTagAction } from "../../actions/cardsActions";
import { Bookmarks, Add } from '@material-ui/icons';
import classNames from "classnames";
import "./style.scss";

export class Tags extends Component {
    constructor(props) {
        super(props);
        this.state = { creating: false };
        this.inputTag = React.createRef();
    }

    componentDidMount() {
        const { tagsFetchAction } = this.props;
        tagsFetchAction();
    }

    handleAdicionar = () => {
        this.setState({ creating: true });
    }

    handleCriar = () => {
        const { tagsAddAction } = this.props;
        const id = new Date();
        tagsAddAction({
            "id": id.getTime(),
            "name": this.inputTag.current.value,
            "color": "#fff",
            "background": "#c483d7"
        })
        this.setState({ creating: false });
    }

    handleSeleciona = (tagId) => {
        const { cardsSelectTagAction } = this.props;
        cardsSelectTagAction(tagId);
    }

    cardPerTag = (tagId) => {
        const { cards} = this.props;
        const filtered = cards.filter(({ tag: tagCard }) => tagCard.filter(tag => tag == tagId).length);
        return filtered.length;
    }

    render() {
        const { tags, loadingTags, loadingCards, selectedTag } = this.props;
        const { creating } = this.state;

        if (loadingTags || loadingCards) {
            return (
                <div>Carregando...</div>
            )
        }

        return (
            <div className="sidebar">
                <h2 className="sidebar__title">Processos</h2>
                <button className={classNames('sidebar__btn--main', { selected: selectedTag == null})} onClick={() => { this.handleSeleciona(null) }}>
                    <Bookmarks className="sidebar__icon"/>Todos os processos
                </button>

                <h3 className="sidebar__title">Etiquetas</h3>
                <ul className="sidebar__tags-list">
                    {tags.map(tag =>
                        <li key={tag.id}>
                            <button className={classNames('sidebar__btn--main', { selected: tag.id == selectedTag })} onClick={() => { this.handleSeleciona(tag.id) }}>
                                <i style={{backgroundColor: tag.background}} className="sidebar__icon-tag"></i>
                                {tag.name}
                                <span>{this.cardPerTag(tag.id)}</span>
                            </button>
                        </li>
                    )}
                </ul>
                {!creating && <div><button className="sidebar__btn--main" onClick={this.handleAdicionar}><Add className="sidebar__icon-create" /> Criar etiqueta</button></div> }
                {creating && <div><input className="sidebar__input-create" type="text" ref={this.inputTag}/><Add className="sidebar__icon-add" onClick={this.handleCriar}/></div> }
            </div>
        )
    }
}

const mapStateToProps = store => ({
    tags: store.tagsReducer.tags,
    loadingTags: store.tagsReducer.loading,
    cards: store.cardsReducer.cards,
    loadingCards: store.cardsReducer.loading,
    selectedTag: store.cardsReducer.selectedTag
});

const mapDispatchToProps = { tagsFetchAction, tagsFetchActionSuccess, tagsAddAction, cardsSelectTagAction };

export default connect(mapStateToProps, mapDispatchToProps)(Tags);

