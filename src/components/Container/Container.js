import React, {Component} from 'react';
import Cards from "../Cards/Cards";
import Search from "../Search/Search";
import Tags from "../Tags/Tags";
import Grid from '@material-ui/core/Grid';
import "./style.scss";


export class Container extends Component {
    render() {
        return (
            <Grid container spacing={0}>
                <Grid item xs={12}>
                    <header>
                        APP
                    </header>
                </Grid>
                <Grid item xs={3}>
                    <Tags/>
                </Grid>
                <Grid item xs={9}>
                    <Search/>
                    <Cards/>
                </Grid>
            </Grid>
        )
    }
}

export default Container;