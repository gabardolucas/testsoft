import React, { Component } from 'react';
import { connect } from 'react-redux';
import { searchAction } from './../../actions/searchActions';
import { Search as SearchIcon } from '@material-ui/icons';
import "./style.scss";

export class Search extends Component {
    constructor(props) {
        super(props);
        this.inputSearch = React.createRef();
    }

    handleSearch = () => {
        const {searchAction} = this.props;
        searchAction(this.inputSearch.current.value);
    }

    render() {
        return (
            <div className="search">
                <SearchIcon className="search__icon"/>
                <input type="text" placeholder="Buscar por assunto, classe ou número" onKeyUp={this.handleSearch} ref={this.inputSearch}/>
            </div>
        )
    }
}

const mapStateToProps = store => ({
    searchTerm: store.searchReducer.searchTerm,
    cards: store.cardsReducer.cards,
});

const mapDispatchToProps = { searchAction };

export default connect(mapStateToProps, mapDispatchToProps)(Search);