import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { fetchCardsAction, fetchCardsActionSuccess, cardsAddTagAction, cardsRemoveTagAction } from './../../actions/cardsActions';
import Grid from '@material-ui/core/Grid';
import { Folder, AddToPhotos, Clear, AddCircle, RemoveCircle} from '@material-ui/icons';
import "./style.scss";

export class Cards extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { fetchCardsAction } = this.props;
        fetchCardsAction();
    }

    handleAddTag = (tagId, cardId) => {
        const { cardsAddTagAction } = this.props;
        cardsAddTagAction(tagId, cardId);
    }

    handleRemoveTag = (tagId, cardId) => {
        const { cardsRemoveTagAction } = this.props;
        cardsRemoveTagAction(tagId, cardId);
    }

    filterCards = () => {
        const { cards, selectedTag, searchTerm} = this.props;

        let filtered = cards;

        if(selectedTag){
            filtered = cards.filter(({ tag: tagCard }) => tagCard.filter(tag => tag == selectedTag).length);
        }

        if(searchTerm){
            const search = filtered.filter(
                            ({ assunto: assuntoCard, classe: classeCard, numero: numeroCard }) =>
                            assuntoCard.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
                            classeCard.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
                            numeroCard.indexOf(searchTerm) > -1);
            return search;
        }else{
            return filtered;
        }
    }

    existInCard(tags, tag) {
        return tags.filter(elm => elm == tag).length;
    }

    render() {
        const { cards, tags, selectedTag, loadingTags, loadingCards, searchTerm} = this.props
        const haveFilter = !!selectedTag;
        const arrCards = haveFilter || searchTerm ? this.filterCards() : cards;

        if (loadingTags || loadingCards) {
            return (
                <div>Carregando...</div>
            )
        }

        return (
            <div className="cards">
                <div className="list">
                    {arrCards.map((card) => {
                        const { id, partes, classe, assunto, numero, tarja, tag: tagCard } = card;

                        return (
                            <div className="card">
                                <Grid container spacing={8}>
                                    <Grid item xs={5}>
                                        <div className="card__partes">
                                            <span><AddCircle  className="card__partes__ativa"/>{partes.ativa.name}</span>
                                            <span><RemoveCircle  className="card__partes__passiva"/>{partes.passiva.name}</span>
                                        </div>
                                        <div><span className="card__classe">{classe}</span> - <span className="card__assunto">{assunto}</span></div>
                                        <div className="card__bottom">
                                            <span className="card__numero">{numero}</span>

                                            <div className="card__tarja-wrapper">
                                                {tarja.map((tarja) => {
                                                    return (
                                                        <span className="card__tarja" style={{backgroundColor: tarja.background,color: tarja.color}} key={tarja.id}>{tarja.name}</span>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </Grid>

                                    <Grid className="card__folder-wrapper" item xs={3}>
                                        <div className="card__folder">
                                            <Folder className="card__folder-icon" /> <span>Abrir pasta</span>
                                        </div>
                                    </Grid>

                                    <Grid className="card__tags-wrapper" item xs={1}>
                                        <AddToPhotos className="card__tags-ico" />
                                        <div className="card__tags-list">
                                            <div>Etiquetar como:</div>
                                            {tags.map((tag) => {
                                                const exist = this.existInCard(tagCard, tag.id)
                                                return (
                                                    <button className="card__tag" style={{backgroundColor: tag.background, color: tag.color}} className={classNames({ hiddenTag: exist })}  disabled={exist} onClick={() => { this.handleAddTag(tag.id, id) }} key={tag.id} value={tag.id}>{tag.name}</button>
                                                )
                                            })}
                                        </div>
                                    </Grid>

                                    <Grid className="card__tags-added" item xs={3}>
                                        <div className="card__tags-added-container">
                                            {tagCard.map((tag) => {
                                                const currentName = tags.find(currentTag => currentTag.id == tag)
                                                return (
                                                    <span style={{backgroundColor: currentName.background, color: currentName.color}}key={tag}>{currentName.name} <Clear className="card__clear-icon" onClick={() => this.handleRemoveTag(tag, id)}/> </span>
                                                )
                                            })}
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = store => ({
    tags: store.tagsReducer.tags,
    loadingTags: store.tagsReducer.loading,
    cards: store.cardsReducer.cards,
    loadingCards: store.cardsReducer.loading,
    selectedTag: store.cardsReducer.selectedTag,
    searchTerm: store.searchReducer.searchTerm,
});

const mapDispatchToProps = {fetchCardsAction, fetchCardsActionSuccess, cardsAddTagAction, cardsRemoveTagAction};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);